package ru.test.springless1;

public class CricketCoach implements Coach {

    private FortuneService fortuneService;

    private String emailAddress;
    private String team;

    public CricketCoach() {
        System.out.println("Cricket coach no-arg constructor");
    }

    public void setFortuneService(FortuneService fortuneService) {
        System.out.println("Setter fortune service");
        this.fortuneService = fortuneService;
    }

    public void setEmailAddress(String emailAddress) {
        System.out.println("Setter email");
        this.emailAddress = emailAddress;
    }

    public void setTeam(String team) {
        System.out.println("Setter team");
        this.team = team;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getTeam() {
        return team;
    }

    public String getDailyWorkout() {
        return "Practice fast";
    }

    public String getDailyFortune() {
        return "Cricket " + fortuneService.getFortune();
    }
}
