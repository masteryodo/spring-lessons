package ru.test.springless1;

public class TrackCoach implements Coach {

    private FortuneService fortuneService;

    public TrackCoach() {
    }

    public TrackCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    public String getDailyWorkout() {
        return "Run a hard 5k";
    }

    public String getDailyFortune() {
        return "Just do it! " + fortuneService.getFortune();
    }

    public void doMyStartup(){
        System.out.println("trackCoach startup method");
    }
    public void doMyDestroy(){
        System.out.println("trackCoach destroy method");
    }
}
