package ru.test.springless1;

public class BaseballCoach implements Coach{

    private FortuneService fortuneService;

    public BaseballCoach(FortuneService myFortuneService) {
        this.fortuneService = myFortuneService;
    }

    public String getDailyWorkout(){
        return "Spend 30 min on batting practiece";
    }

    public String getDailyFortune() {
        return fortuneService.getFortune();
    }
}
