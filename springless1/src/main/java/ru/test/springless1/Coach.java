package ru.test.springless1;

public interface Coach {

    String getDailyWorkout();
    String getDailyFortune();
}
